+++
# About/Biography widget.

date = "2016-04-20T00:00:00"
draft = false

widget = "about"

# Order that this section will appear in.
weight = 1

# List your academic interests.
[interests]
  interests = [
    "Artificial Intelligence",
    "Computational Linguistics",
    "Information Retrieval"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "PhD in Artificial Intelligence"
  institution = "Stanford University"
  year = 2012

[[education.courses]]
  course = "MEng in Artificial Intelligence"
  institution = "Massachusetts Institute of Technology"
  year = 2009

[[education.courses]]
  course = "BSc in Artificial Intelligence"
  institution = "Massachusetts Institute of Technology"
  year = 2008
 
+++

# Biography

## The Digital Universe Is A Vast Galaxy.
## Where Do I Begin My Journey?


You must Establish Your Digital Identity in order to participate in the future.
An Autobiographical Portfolio Is A Launchpad To Explore The Digital Universe!
An Autobiographical Web Portfolio is an expression of yourself.
Why do we create content for others to use and not benefit ourselves?

DW84 Inc LLC Foundation Retention Agent primary role is initiate contact with the client and advocate Foundation Goals plus Vision.
